import React, { useState, useEffect } from 'react';

function HatList() {
  const [hats, setHats] = useState([]);

  const fetchData = async () => {
      const url = "http://localhost:8090/api/hats/";
      const response = await fetch(url);

      if (response.ok) {
          const data = await response.json();
          console.log(data.hats, 'data')
          setHats(data.hats);
        }
    };

    const handleDelete = async (event, hatId) => {
        event.preventDefault();

        const hatUrl = `http://localhost:8090/api/hats/${hatId}`;
        const config = {
          method: "DELETE",
        };

        const response = await fetch(hatUrl, config);

        if (response.ok) {
            const newHat = await response.json();
            console.log(newHat);
        }
    }

useEffect(() => {
    fetchData();
}, []);

  return (
    <div className="row">
      <div className="col-12">
        <h1>Hats</h1>
        <table className="table">
          <thead>
            <tr>
              <th>Style Name</th>
              <th>Fabric</th>
              <th>Color</th>
              <th>Location</th>
              <th>Picture</th>
              <th>Delete</th>
            </tr>
          </thead>
          <tbody>
            {hats.map((hat) => (
              <tr key={hat.id}>
                <td>{hat.style_name}</td>
                <td>{hat.fabric}</td>
                <td>{hat.color}</td>
                <td>{hat.location.closet_name}</td>
                <td>
                  <img src={hat.picture_url} alt={`${hat.style_name} ${hat.fabric}`} width="100" />
                </td>
                <td>
                <button onClick={(event) => {handleDelete(event, hat.id)}}>Delete</button>
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    </div>
  );
}
export default HatList;
